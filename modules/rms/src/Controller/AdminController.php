<?php
/**
@file
Contains \Drupal\rms\Controller\AdminController.
 */

namespace Drupal\rms\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\rms\RmsStorage;

class AdminController extends ControllerBase {

function contentOriginal() {
  $url = Url::fromRoute('rms_add');
  //$add_link = ;
  $add_link = '<p>' . \Drupal::l(t('New message'), $url) . '</p>';

  // Table header
  $header = array( 'id' => t('Id'), 'name' => t('Submitter name'), 'message' => t('Message'), 'operations' => t('Delete'), );

  $rows = array();
  foreach(RmsStorage::getAll() as $id=>$content) {
    // Row with attributes on the row and some of its cells.
    $rows[] = array( 'data' => array($id, $content->name, $content->message, l('Delete', "admin/content/rms/delete/$id")) );
   }

   $table = array( '#type' => 'table', '#header' => $header, '#rows' => $rows, '#attributes' => array( 'id' => 'bd-contact-table', ), );
   return $add_link . drupal_render($table);
 }

  public function content1() {
    return array(
      '#type' => 'markup',
      '#markup' => t('Hello World'),
    );
  }

  function content() {
    $url = Url::fromRoute('rms_add');
    //$add_link = ;
    $add_link = '<p>' . \Drupal::l(t('New message'), $url) . '</p>';

    $text = array(
      '#type' => 'markup',
      '#markup' => $add_link,
    );

    // Table header.
    $header = array(
      'id' => t('Id'),
      'name' => t('Submitter name'),
      'message' => t('Message'),
      'operations' => t('Delete'),
    );
    $rows = array();
    foreach (RmsStorage::getAll() as $id => $content) {
      // Row with attributes on the row and some of its cells.
      $editUrl = Url::fromRoute('rms_edit', array('id' => $id));
      $deleteUrl = Url::fromRoute('rms_delete', array('id' => $id));

      $rows[] = array(
        'data' => array(
          \Drupal::l($id, $editUrl),
          $content->name, $content->message,
          \Drupal::l('Delete', $deleteUrl)
        ),
      );
    }
    $table = array(
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => array(
        'id' => 'bd-contact-table',
      ),
    );
    //return $add_link . ($table);
    return array(
      $text,
      $table,
    );
  }
}
