<?php

namespace Drupal\ranosys;

class RanosysStorage {

  static function getAll() {
    $result = db_query('SELECT * FROM {ranosys}')->fetchAllAssoc('id');
    return $result;
  }

  static function exists($id) {
    return (bool) $this->get($id);
  }

  static function get($id) {
    $result = db_query('SELECT * FROM {ranosys} WHERE id = :id', array(':id' => $id))->fetchAllAssoc('id');
    if ($result) {
      return $result[$id];
    }
    else {
      return FALSE;
    }
  }

  static function add($name, $message) {
    db_insert('ranosys')->fields(array(
      'name' => $name,
      'message' => $message,
    ))->execute();
  }

  static function edit($id, $name, $message) {
    db_update('ranosys')->fields(array(
      'name' => $name,
      'message' => $message,
    ))
    ->condition('id', $id)
    ->execute();
  }
  
  static function delete($id) {
    db_delete('ranosys')->condition('id', $id)->execute();
  }
}
