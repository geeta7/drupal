<?php

/**
 * @file
 * Contains \Drupal\status_update\Controller\NodeController.
 */

namespace Drupal\status_update\Controller;

use Drupal\Component\Utility\String;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\status_update\Entity\StatusUpdate;

/**
 * Returns responses for status updates.
 */
class StatusUpdateController extends ControllerBase {

  /**
   * Content callback for the 'create new status update' page.
   * @return array
   */
  public function add() {
    $user = \Drupal::currentUser();
    $status_update = entity_create('status_update', array(
      'uid' => $user->id(),
      'created' => REQUEST_TIME,
      'message' => '',
    ));
    return \Drupal::entityManager()->getForm($status_update);
  }

}