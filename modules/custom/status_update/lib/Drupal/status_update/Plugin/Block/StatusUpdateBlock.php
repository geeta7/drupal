<?php

/**
 * @file
 * Contains \Drupal\status_update\Plugin\Block\StatusUpdateBlock.
 */

namespace Drupal\status_update\Plugin\Block;

use Drupal\block\Annotation\Block;
use Drupal\block\BlockBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zend\Stdlib\Request;

/**
 * Provides a 'Status Update' block.
 *
 * @Block(
 *   id = "status_update_block",
 *   admin_label = @Translation("Latest status updates")
 * )
 */
class StatusUpdateBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Load the latest status updates.
    $query = db_select('status_update', 'su')
      ->fields('su', array('suid'))
      ->orderBy('su.created', 'DESC')
      ->range(0, $this->getBlockCount());
    $suids = $query->execute()->fetchCol(0);

    $status_updates = entity_load_multiple('status_update', $suids);

    // Theme them.
    $render = array(
      'status_updates' => array(
        '#theme' => 'item_list',
        '#items' => array(),
      ),
    );

    foreach ($status_updates as $entity) {
      $item = array(
        'username' => array(
          '#prefix' => t('Posted by '),
          '#theme' => 'username',
          '#account' => ($entity->getAuthor() ? $entity->getAuthor() : user_load(0)),
        ),
        'created' => array(
          '#prefix' => t(' on '),
          '#markup' => check_plain(format_date($entity->getCreated())),
        ),
        'message' => array(
          '#markup' => '<p class="message">' . nl2br(check_plain($entity->getMessage())) . '</p>',
        ),
      );

      $render['status_updates']['#items'][] = $item;
    }


    // Add links.
    if (\Drupal::currentUser()->hasPermission('create status updates')) {
      $render['new_status_update_link'] = array(
        '#markup' => l(t('post status update'), 'status-update/add', array('attributes' => array('class' => array('new-status-update')))),
      );
    }

    $render['more_link'] = array(
      '#theme' => 'more_link',
      '#url' => 'status-updates',
      '#title' => t('more'),
    );

    return $render;
  }

  /**
   * Overrides \Drupal\block\BlockBase::access().
   */
  public function access(AccountInterface $account) {
    return $account->hasPermission('view status updates');
  }

  /**
   * Returns the block count.
   */
  protected function getBlockCount() {
    if (!empty($this->configuration['block_count'])) {
      return $this->configuration['block_count'];
    } else {
      // Get module config setting.
      return \Drupal::config('status_update.settings')->get('default_n_status_updates_block');
    }
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockForm().
   */
  public function blockForm($form, &$form_state) {
    $form['block_count'] = array(
      '#type' => 'select',
      '#title' => t('Number of status updates'),
      '#default_value' => $this->configuration['block_count'],
      '#options' => array('' => t('Use site-wide default (see module settings)')) + drupal_map_assoc(range(2, 20)),
    );
    return $form;
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockSubmit().
   */
  public function blockSubmit($form, &$form_state) {
    $this->configuration['block_count'] = $form_state['values']['block_count'];
  }

}