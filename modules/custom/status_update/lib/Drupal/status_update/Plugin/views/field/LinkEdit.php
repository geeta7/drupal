<?php

/**
 * @file
 * Definition of Drupal\status_update\Plugin\views\field\LinkEdit.
 */

namespace Drupal\status_update\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Drupal\Component\Annotation\PluginID;

/**
 * Field handler to present a status update edit link.
 *
 * @ingroup views_field_handlers
 *
 * @PluginID("status_update_link_edit")
 */
class LinkEdit extends FieldPluginBase {

  /**
   * Overrides Drupal\views\Plugin\views\field\FieldPluginBase::init().
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    $this->additional_fields['suid'] = 'suid';
  }

  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['text'] = array('default' => '', 'translatable' => TRUE);

    return $options;
  }

  public function buildOptionsForm(&$form, &$form_state) {
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
    parent::buildOptionsForm($form, $form_state);
  }

  public function query() {
    $this->ensureMyTable();
    $this->addAdditionalFields();
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    // Check there is an actual value, as on a relationship there may not be.
    if ($suid = $this->getValue($values, 'suid')) {
      $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');
      return l($text, 'status-update/'. $suid . '/edit', array('query' => drupal_get_destination()));
    }
  }

}