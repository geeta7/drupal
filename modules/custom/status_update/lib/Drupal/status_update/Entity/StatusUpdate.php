<?php

/**
 * @file
 * Definition of Drupal\status_update\Entity\StatusUpdate.
 */

namespace Drupal\status_update\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageControllerInterface;
use Drupal\user\Entity\User;

/**
 * Defines the status update entity class.
 *
 * @EntityType(
 *   id = "status_update",
 *   label = @Translation("Status Update"),
 *   module = "status_update",
 *   controllers = {
 *     "storage" = "Drupal\Core\Entity\FieldableDatabaseStorageController",
 *     "render" = "Drupal\Core\Entity\EntityRenderController",
 *     "form" = {
 *       "default" = "Drupal\status_update\StatusUpdateFormController",
 *       "delete" = "Drupal\status_update\Form\StatusUpdateDeleteForm"
 *     }
 *   },
 *   base_table = "status_update",
 *   fieldable = TRUE,
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "suid",
 *     "uuid" = "uuid",
 *     "label" = "message",
 *   },
 *   route_base_path = "admin/config/content/status_update",
 *   links = {
 *     "edit-form" = "status_update.edit",
 *     "admin-form" = "status_update.settings"
 *   }
 * )
 */
class StatusUpdate extends ContentEntityBase implements ContentEntityInterface {

  /**
   * Implements Drupal\Core\Entity\EntityInterface::id().
   */
  public function id() {
    return $this->get('suid')->value;
  }

  /**
   * Returns the currently set message.
   * @return string
   */
  public function getMessage() {
    return $this->get('message')->value;
  }

  /**
   * Sets the message.
   */
  public function setMessage($message) {
    $this->set('message', $message);
    return $this;
  }

  /**
   * Returns the currently set created timestamp.
   * @return int
   */
  public function getCreated() {
    return $this->get('created')->value;
  }

  /**
   * Sets the message.
   */
  public function setCreated($created) {
    $this->set('created', $created);
    return $this;
  }

  /**
   * Returns the author user.
   * @return User
   */
  public function getAuthor() {
    return $this->get('uid')->entity;
  }

  /**
   * Returns the author user id.
   */
  public function getAuthorId() {
    return $this->get('uid')->target_id;
  }

  /**
   * Sets the author user id.
   * @param $uid
   * @return $this
   */
  public function setAuthorId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions($entity_type) {
    $fields['suid'] = array(
      'label' => t('ID'),
      'description' => t('The ID of the status update.'),
      'type' => 'integer_field',
      'read-only' => TRUE,
    );
    $fields['uuid'] = array(
      'label' => t('UUID'),
      'description' => t('The status update UUID.'),
      'type' => 'uuid_field',
      'read-only' => TRUE,
    );
    $fields['uid'] = array(
      'label' => t('User ID'),
      'description' => t('The user of the status update author.'),
      'type' => 'entity_reference_field',
      'settings' => array(
        'target_type' => 'user',
        'default_value' => 0,
      ),
    );
    $fields['langcode'] = array(
      'label' => t('Language code'),
      'description' => t('The status update language code.'),
      'type' => 'language_field',
    );
    $fields['message'] = array(
      'label' => t('Message'),
      'description' => t('The message of the status update.'),
      'type' => 'string_field',
    );
    $fields['created'] = array(
      'label' => t('Checked'),
      'description' => t('Last time feed was checked for new items, as Unix timestamp.'),
      'type' => 'integer_field',
    );


    return $fields;
  }

}

