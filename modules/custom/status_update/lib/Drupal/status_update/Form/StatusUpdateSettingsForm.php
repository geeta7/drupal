<?php

/**
 * @file
 * Contains \Drupal\status_update\StatusUpdateSettingsForm.
 */

namespace Drupal\status_update\Form;

use Drupal\Core\Form\ConfigFormBase;

/**
 * Configure status update settings for this site.
 */
class StatusUpdateSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'status_update_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $config = $this->configFactory->get('status_update.settings');

    $form['default_n_status_updates_block'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Status updates per block'),
      '#default_value' => $config->get('default_n_status_updates_block'),
      '#description' => $this->t('Default number of status updates displayed per block.'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
    if (!preg_match("/^\d+$/", $form_state['values']['default_n_status_updates_block'])) {
      form_set_error('default_n_status_updates_block', $form_state, $this->t('Specify an integer value'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $this->configFactory->get('status_update.settings')
      ->set('default_n_status_updates_block', $form_state['values']['default_n_status_updates_block'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
