<?php

/**
 * @file
 * Contains \Drupal\status_update\StatusUpdateFormController.
 */

namespace Drupal\status_update;

use Drupal\Component\Utility\String;
use Drupal\Core\Entity\ContentEntityFormController;
use Drupal\Core\Language\Language;

/**
 * Form controller for the status update edit forms.
 */
class StatusUpdateFormController extends ContentEntityFormController {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, array &$form_state) {
    /**
     * @var \Drupal\status_update\Entity\StatusUpdate $status_update
     */
    $status_update = $this->entity;

    if (!$this->entity->isNew()) {
      // We need to set the title manually in this case.
      drupal_set_title(t('Edit status update <em>@title</em>', array('@title' => $status_update->label())), PASS_THROUGH);
    } else {
      drupal_set_title(t('Create new status update'), PASS_THROUGH);
    }

    $form['message'] = array(
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('Message'),
      '#default_value' => $status_update->message,
      '#description' => $this->t('The message itself.'),
      '#default_value' => $status_update->getMessage(),
    );

    $form['author'] = array(
      '#type' => 'textfield',
      '#title' => t('Authored by'),
      '#maxlength' => 60,
      '#autocomplete_route_name' => 'user.autocomplete',
      '#default_value' => $status_update->getAuthorId() ? $status_update->getAuthor()->getUsername() : '',
      '#access' => \Drupal::currentUser()->hasPermission('administer status updates'),
    );

    return parent::form($form, $form_state, $status_update);
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::validate().
   */
  public function validate(array $form, array &$form_state) {
    // Validate the "authored by" field.
    if (!empty($form_state['values']['author']) && !($account = user_load_by_name($form_state['values']['author']))) {
      form_set_error('author', t('The username %name does not exist.', array('%name' => $form_state['values']['author'])));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, array &$form_state) {
    $entity = parent::buildEntity($form, $form_state);

    // A user might assign the node author by entering a user name in the node
    // form, which we then need to translate to a user ID.
    if (!empty($form_state['values']['author']) && $account = user_load_by_name($form_state['values']['author'])) {
      $entity->setAuthorId($account->id());
    }
    else {
      $entity->setAuthorId(0);
    }

    return $entity;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   */
  public function save(array $form, array &$form_state) {
    $insert = $this->entity->isNew();

    $this->entity->save();

    if ($insert) {
      watchdog('content', 'Status update: added %title.', array('%title' => $this->entity->label()), WATCHDOG_NOTICE);
      drupal_set_message(t('Status update %title has been created.', array('%title' => $this->entity->label())));
    }
    else {
      watchdog('content', 'Status update: updated %title.', array('%title' => $this->entity->label()), WATCHDOG_NOTICE);
      drupal_set_message(t('Status update %title has been updated.', array('%title' => $this->entity->label())));
    }

    if ($insert) {
      // Go to home page.
      $form_state['redirect'] = array('');
    } else {
      $form_state['redirect'] = array('status-update/' . $this->entity->id() . '/edit');
    }
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::delete().
   */
  public function delete(array $form, array &$form_state) {
    $destination = array();
    $query = \Drupal::request()->query;
    if ($query->has('destination')) {
      $destination = drupal_get_destination();
      $query->remove('destination');
    }
    $form_state['redirect'] = array('status-update/' . $this->entity->id() . '/delete', array('query' => $destination));
  }

}
