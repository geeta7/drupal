<?php

/**
 * @file
 * Provide views data and handlers for status_update.module.
 *
 * @ingroup views_module_handlers
 */

use Drupal\Core\Entity\FieldableDatabaseStorageController;
use Drupal\field\FieldInterface;

/**
 * Implements hook_views_data().
 */
function status_update_views_data() {
  $data = array();

  $data['status_update']['table']['group']  = t('Status update');
  $data['status_update']['table']['base'] = array(
    'field' => 'suid',
    'title' => t('Status Update'),
  );
  $data['status_update']['table']['entity type'] = 'status_update';

  $data['status_update']['suid'] = array(
    'title' => t('Status update ID'),
    'help' => t('The suid of a status update.'),
    'field' => array(
      'id' => 'numeric',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'title' => t('Status update'),
      'help' => t('Status update suid.'),
      'id' => 'numeric',
      'allow empty' => TRUE,
    ),
  );

  $data['status_update']['message'] = array(
    'title' => t('Message'),
    'help' => t('The status update message itself.'),
    'field' => array(
      'id' => 'standard',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'string',
    ),
    'argument' => array(
      'id' => 'string',
    ),
  );

  $data['status_update']['created'] = array(
    'title' => t('Post date'),
    'help' => t('The date the status update was posted.'),
    'field' => array(
      'id' => 'date',
    ),
    'sort' => array(
      'id' => 'date'
    ),
    'filter' => array(
      'id' => 'date',
    ),
  );

  $data['status_update']['uid'] = array(
    'title' => t('Author uid'),
    'help' => t('The user authoring the status update. If you need more fields than the uid add the content: author relationship'),
    'relationship' => array(
      'title' => t('Status update author'),
      'help' => t('Relate status update to the user who created it.'),
      'id' => 'standard',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('author'),
    ),
    'filter' => array(
      'id' => 'user_name',
    ),
    'argument' => array(
      'id' => 'numeric',
    ),
    'field' => array(
      'id' => 'user',
    ),
  );

  // Link to edit the status update
  $data['status_update']['edit_status_update'] = array(
    'field' => array(
      'title' => t('Status update edit link'),
      'help' => t('Provide a simple link to edit the status update.'),
      'id' => 'status_update_link_edit',
      'click sortable' => FALSE,
    ),
  );

  return $data;
}